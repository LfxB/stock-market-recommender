const reducer = (state = {}, action) => {
    switch (action.type) {
        case 'symbol.select':
            return { ...action.data };
        default:
            return state;
    }
}

// actions
export const selectSymbol = (symbolData) => {
    return (dispatch) => {
        dispatch({type: 'symbol.select', data: symbolData});
    }
}

export default reducer;