const reducer = (state = 10, action) => {
    switch (action.type) {
        case 'time.window':
            return action.days;
        default:
            return state;
    }
}

// actions
export const setTimeWindow = (days) => {
    let parsed = parseInt(days);

    return (dispatch) => {
        dispatch({type: 'time.window', days: isNaN(parsed) ? 10 : parsed});
    }
}

export default reducer;