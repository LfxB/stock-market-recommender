import { ALGORITHM_TYPES } from 'utils/algorithms';

const reducer = (state = ALGORITHM_TYPES[0], action) => {
    switch (action.type) {
        case 'algorithm.select':
            return { ...action.data };
        default:
            return state;
    }
}

// actions
export const selectAlgorithm = (algoData) => {
    return (dispatch) => {
        dispatch({type: 'algorithm.select', data: algoData});
    }
}

export default reducer;