import React from 'react';
import 'components/MainContainer.css'

export default class MainContainer extends React.Component {
    render = () => {
        return (
            <div className="main-container">
                <h1 className="main-container-header">Stock Market Recommender</h1>
                <div className="children-container">
                    {this.props.children}
                </div>
            </div>
        );
    }
}
