import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import { routerMiddleware, connectRouter } from 'connected-react-router';

import selectedSymbol from 'modules/selectedSymbol';
import timeWindow from 'modules/timeWindow';
import selectedAlgorithm from 'modules/selectedAlgorithm'

const rootReducer = (history) => combineReducers({
    router: connectRouter(history),
    // Insert reducers here
    selectedSymbol,
    timeWindow,
    selectedAlgorithm
});

const configureStore = (initialState = {}, history) => {
    // List middleware here
    const middlewares = [
        routerMiddleware(history),
        thunk,
    ];

    // Add support for Redux dev tools
    const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

    const store = createStore(
        rootReducer(history),
        initialState,
        composeEnhancers(
            applyMiddleware(...middlewares)
        )
    );

    return store;
};

export default configureStore;
