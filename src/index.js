import React from 'react';
import { render } from 'react-dom';
import { Route, Switch } from 'react-router-dom';
import * as serviceWorker from './serviceWorker';

// redux specific imports
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import { createBrowserHistory } from 'history';
import configureStore from './store';

// Global styles
import './index.css';

import Home from 'pages/Home';

const initialState = {};
const history = createBrowserHistory();
const store = configureStore(initialState, history);

render(
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <Switch>
                <Route exact path='/' component={Home} />
            </Switch>
        </ConnectedRouter>
    </Provider>,
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
