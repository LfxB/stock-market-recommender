import React from 'react';

import MainContainer from 'components/MainContainer';
import Options from "./home/Options";
import Results from './home/Results';

export default class Home extends React.Component {
    render = () => {
        return (
            <MainContainer>
                <Options />
                <Results />
            </MainContainer>
        );
    }
}
