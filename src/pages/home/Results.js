import React from 'react';
import {connect} from 'react-redux';

import { SOCIAL_MEDIA_TYPES } from 'utils/socialMedia';

import {
    recommendationAlgorithm,
    stockPriceGenerator,
    socialMediaCountGenerator
} from 'utils/gateway';

import './Results.css'

class Results extends React.Component {
    createTable = () => {
        const { symbolData, timeWindow, selectedAlgorithm } = this.props;

        if (!symbolData.symbol) return null;

        let stockPrices = stockPriceGenerator(symbolData.symbol, timeWindow, SOCIAL_MEDIA_TYPES)
        let socialMediaCount = socialMediaCountGenerator(symbolData.symbol, SOCIAL_MEDIA_TYPES)
        let recommendations = recommendationAlgorithm(stockPrices, socialMediaCount, symbolData.symbol, selectedAlgorithm)
        // console.log("Recommendations: ", recommendations);

        return (
            <table className="recommendations-table">
                <tbody>
                    <tr>
                        <th>Date</th>
                        <th>Price</th>
                        <th>Post Count</th>
                        <th>Recommendation</th>
                    </tr>
                </tbody>
                {recommendations.map((item, key) => {
                    return (
                        <tbody key={key}>
                            <tr>
                                <td>{item.date}</td>
                                <td>{"$" + item.price}</td>
                                <td>{item.totalPostCount}</td>
                                <td>{item.recommendation}</td>
                            </tr>
                        </tbody>
                    )
                })}
            </table>
        )
    }

    render = () => {
        return (
            <div className="results-container">
                {this.createTable()}
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        symbolData: state.selectedSymbol,
        timeWindow: state.timeWindow,
        selectedAlgorithm: state.selectedAlgorithm
    }
}

export default connect(mapStateToProps)(Results);