import React from 'react';
import {connect} from 'react-redux';

import { TIME_WINDOW_DAYS } from 'utils/timeWindows';
import { setTimeWindow } from 'modules/timeWindow';

import './TimeWindowSelector.css';

class TimeWindowSelector extends React.Component {
    onDayClick = (event, day) => {
        event.preventDefault();
        this.props.dispatch(setTimeWindow(day))
    }

    render = () => {
        const { timeWindow } = this.props;

        return (
        <div className="timewindow-container">
            {TIME_WINDOW_DAYS.map((day, key) => {
                return (
                    <button
                    key={key}
                    className={timeWindow === day ? "timewindow__btn timewindow__btn--selected" : "timewindow__btn"}
                    onClick={(event) => this.onDayClick(event, day)}
                    >
                        {day + "D"}
                    </button>
                )
            })}
        </div>)
    }
}

const mapStateToProps = (state) => {
    return {
        timeWindow: state.timeWindow
    }
}

export default connect(mapStateToProps)(TimeWindowSelector);