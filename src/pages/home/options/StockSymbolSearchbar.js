import React from 'react';
import {connect} from 'react-redux';
import { getStockSymbolSearchResults } from 'utils/gateway';
import { selectSymbol } from 'modules/selectedSymbol';

import "./StockSymbolSearchbar.css"

class StockSymbolSearchbar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            input: '',
            searchResults: {},
            inputFocus: false
        }
    }

    componentDidMount = () => {
        const searchResults = getStockSymbolSearchResults('');
        this.setState({
            searchResults
        })
    }

    toggleInputFocus = (val, timeout) => {
        if (timeout === undefined) {
            this.setState({
                inputFocus: val
            })
        } else {
            this.timeoutid = setTimeout(() => {
                this.setState({
                    inputFocus: val
                })
            }, timeout)
        }
    }

    onInputChange = (event) => {
        const input = event.target.value;
        const searchResults = getStockSymbolSearchResults(input);
        this.setState({
            input,
            searchResults
        })
    }

    onSymbolSelect = (data) => {
        this.props.dispatch(selectSymbol(data));
        clearTimeout(this.timeoutid);
        this.setState({
            input: data.symbol,
            inputFocus: false
        })
    }

    getDatalist = () => {
        const { input, searchResults, inputFocus } = this.state;

        if (!inputFocus) return null;
        if (!searchResults) return null;
        if (!searchResults["symbols_returned"]) return null;
        if (parseInt(searchResults["symbols_returned"]) === 0) return null;

        let items = searchResults.data.filter(item => {
            return item.symbol.toLowerCase().includes(input.toLowerCase());
        })

        return (
            <div className="searchbar-list">
                {items.map((item, key) => {
                    return (
                    <div className="searchbar-list-item" key={key} onClick={() => this.onSymbolSelect(item)}>
                        <div className="searchbar-list-item-symbol-name-container">
                            <div className="searchbar-list-item-symbol">{item.symbol}</div>
                            <div className="searchbar-list-item-name">{item.name}</div>
                        </div>
                        <div className="searchbar-list-item-price">{'$' + item.price}</div>
                    </div>
                    )
                })}
            </div>
        )
    }

    render = () => {
        return (
            <div className="searchbar-container">
                <input
                className="searchbar-input"
                type="text"
                value={this.state.input}
                onChange={this.onInputChange}
                onFocus={() => this.toggleInputFocus(true)}
                onBlur={() => this.toggleInputFocus(false, 200)}
            />
                {this.getDatalist()}
            </div>
        );
    }
}

export default connect()(StockSymbolSearchbar)