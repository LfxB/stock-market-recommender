import React from 'react';
import {connect} from 'react-redux';

import { selectAlgorithm } from 'modules/selectedAlgorithm';
import { ALGORITHM_TYPES } from 'utils/algorithms';

import './AlgorithmSelector.css'

class Results extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            selectValue: 0
        }
    }

    onChangeExtraParamInput = (event, key) => {
        const { selectedAlgorithm } = this.props;

        let modifiedParams = [ ...selectedAlgorithm.params ];

        modifiedParams[key].input = event.target.value;

        this.props.dispatch(selectAlgorithm({
            ...selectedAlgorithm,
            params: modifiedParams
        }));
    }

    getExtraParams = () => {
        const { selectedAlgorithm } = this.props;

        if (selectedAlgorithm.params.length === 0) {
            return null;
        }

        return (
            <div className="algorithm-selector-extra-container">
                {selectedAlgorithm.params.map((param, key) => {
                    return (
                        <div key={key} className="algorithm-selector-extra">
                            <h4>{param.name}</h4>
                            <input
                                type="text"
                                value={param.input}
                                onChange={(event) => this.onChangeExtraParamInput(event, key)}
                            />
                            <p>{param.help}</p>
                        </div>
                    )
                })}
            </div>
        )
    }

    onSelectChange = (event) => {
        this.setState({
            selectValue: event.target.value
        })
        this.props.dispatch(selectAlgorithm(ALGORITHM_TYPES[event.target.value]));
    }

    render = () => {
        return (
            <div className="algorithm-selector-container">
                <div className="algorithm-selector-label">Algorithm</div>
                <select
                    className="algorithm-selector-select"
                    value={this.state.selectValue}
                    onChange={this.onSelectChange}
                >
                {ALGORITHM_TYPES.map((type, key) => {
                    return (
                    <option
                        key={key}
                        value={key}
                    >
                    {type.name}
                    </option>
                    )
                })}
                </select>
                {this.getExtraParams()}
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        selectedAlgorithm: state.selectedAlgorithm
    }
}

export default connect(mapStateToProps)(Results);