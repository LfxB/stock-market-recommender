import React from 'react';

import StockSymbolSearchbar from "./options/StockSymbolSearchbar";
import TimeWindowSelector from "./options/TimeWindowSelector";
import AlgorithmSelector from "./options/AlgorithmSelector";

export default class Options extends React.Component {
    render = () => {
        return (
            <div className="options-container">
                <StockSymbolSearchbar />
                <TimeWindowSelector />
                <AlgorithmSelector />
            </div>
        );
    }
}
