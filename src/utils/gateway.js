import { SAMPLE_RESULTS } from "./sampleTickerResults";
import { createAlgoQuery } from "./algorithms";
import { getRandomInt } from "utils/numberHelper";

/*
    Returns an object containing symbol data that match
    the given name.
    params: name    : string
    return type: object
*/
export const getStockSymbolSearchResults = (name) => {

    // ex:
    // fetch("https://api.worldtradingdata.com/api/v1/stock?symbol=AAPL&api_token=t0k3niD")
    // .then(response => response.text())
    // .then(responseBody => {
    //     console.log(responseBody);
    // })

    return { ...SAMPLE_RESULTS };
}

const getRecommendation = (number) => {
    if (number > 0) {
        return 'Buy'
    }
    if (number < 0) {
        return 'Sell'
    }
    return 'Hold'
}

/*
    Returns an array of objects with stock prices, date,
    post counts (and from which social media service), and
    recommendations to buy, hold, or sell.
    params: stockPrices         : array
            socialMediaCounts   : array
            symbolName          : string
            algoData            : object
    return type: array
*/
export const recommendationAlgorithm = (stockPrices, socialMediaCounts, symbolName, algoData) => {
    // Create query and then fetch from server.
    const algoQuery = createAlgoQuery(symbolName, algoData);
    console.log("Created algo query to send to server: ", algoQuery)

    // Create mock data
    let today = new Date();
    let dateOptions = { year: 'numeric', month: 'numeric', day: 'numeric' };

    stockPrices.unshift({
        price: (Math.random() * getRandomInt(20, 500)).toFixed(2),
        date: today.toLocaleDateString("en-US", dateOptions),
        postCounts: socialMediaCounts,

    });

    let results = stockPrices.map((stock, index) => {
        
        let postCountNow = stock.postCounts.reduce((prev, curr) => {
            return { posts: (prev.posts + curr.posts) }
        }).posts

        let postCountBefore = stockPrices[Math.max(0, index - 1)].postCounts.reduce((prev, curr) => {
            return { posts: (prev.posts + curr.posts) }
        }).posts

        let countDiff = postCountNow - postCountBefore;

        let calcRecommendation = () => {
            if (algoData.name === 'Basic') {
                return getRecommendation(countDiff);
            } else if (algoData.name === 'Risk/Reward') {
                return getRecommendation(countDiff * 0.5);
            }
        }

        return {
            ...stock,
            totalPostCount: postCountNow,
            recommendation: calcRecommendation()
        }
    })

    // console.log("Results: ", results)
    return results;
}

/*
    Currently returns an array of objects with stock prices for each date.
    params: stockSymbol         : string
            pastDaysCount       : number
            socialMediaTypes    : array of strings
    return type: array of objects
*/
export const stockPriceGenerator = (stockSymbol, pastDaysCount, socialMediaTypes) => {
    let today = new Date();
    let dateOptions = { year: 'numeric', month: 'numeric', day: 'numeric' };
    let ret = [];
    for (let n = 1; n < pastDaysCount; n++) {
        
        let postCounts = [];

        for (let i = 0; i < socialMediaTypes.length; i++) {
            postCounts.push({
                service: socialMediaTypes[i].name,
                posts: getRandomInt(1, 1200)
            })
        }

        ret.push({
            price: (Math.random() * getRandomInt(20, 500)).toFixed(2),
            date: new Date(today.getTime() - (n * 24 * 60 * 60 * 1000)).toLocaleDateString("en-US", dateOptions),
            postCounts
        });
    }
    return ret;
}

/*
    Gets the number of posts mentioning the given stock symbol
    within the given social media types/services.
    params: stockSymbol         : string
            socialMediaTypes    : array of strings
    return type: array
*/
export const socialMediaCountGenerator = (stockSymbol, socialMediaTypes) => {
    let postCounts = [];

    for (let i = 0; i < socialMediaTypes.length; i++) {
        postCounts.push({
            service: socialMediaTypes[i].name,
            posts: getRandomInt(1, 1200)
        })
    }
    return postCounts;
}

