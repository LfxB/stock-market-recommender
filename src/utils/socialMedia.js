export const SOCIAL_MEDIA_TYPES = [
    {
        name: 'Twitter',
        queryName: 'twitter'
    },
    {
        name: 'Facebook',
        queryName: 'facebook'
    },
    {
        name: 'LinkedIn',
        queryName: 'linkedin'
    }
]