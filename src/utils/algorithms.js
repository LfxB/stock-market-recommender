export const ALGORITHM_TYPES = [
    {
        name: 'Basic',
        queryName: 'basic',
        params: []
    },
    {
        name: 'Risk/Reward',
        queryName: 'riskreward',
        params: [
            {
                name: 'Risk/Reward Ratio',
                queryName: 'riskrewardratio',
                type: 'string',
                help: 'Ex: Your risk/reward ratio is "1:2" if you stand to make double the amount you risk.',
                input: '1:2'
            },
            {
                name: 'Min Price',
                queryName: 'minprice',
                type: 'string',
                help: 'Ex: If the price goes below this value, automatically recommend to HOLD.',
                input: '23.36'
            }
        ]
    }
]

export const createAlgoQuery = (symbolName, algoData) => {
    let query = '?';
    query += 'symbol=' + symbolName;
    query += '&type=' + algoData.queryName;
    algoData.params.forEach(param => {
        query += '&' + param.queryName + '=' + encodeURIComponent(param.input);
    })
    return query;
}