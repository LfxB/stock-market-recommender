## LogMeIn Coding Challenge

### Code Organisation

I decided to use a modification of the [Fractal structure](https://hackernoon.com/fractal-a-react-app-structure-for-infinite-scale-4dab943092af).

- src.pages are the root level components, ones which are directly mounted on level 1 routes. (Ex. if you have a route called /login that mounts a Login component, then Login.js will be present in pages directory).
- src.modules handles your state (actions + reducers using ducks file structure).
- src.components have shared components, like Button, Input etc.
- src.utils have utilities like your API wrapper, date utils, string utils etc.
- store initializes the redux store.
- index registers the routes and renders the app.

Basically this is a component-node tree structure.
There are no presentational components and container components.
There are just shared and non-shared components.

#### Shared components
- MainContainer.js (contains the header)

#### Pages
- Home.js
   - Options.js
      - Stock Symbol search bar/selector (.js)
      - Time window selector (.js)
      - Social media type selector (todo) (.js)
      - Algorithm selector (.js)
    - Results component to show stock symbol prices and post counts per social media types for last X amount of days, as per the time window selector (.js)

#### Modules

- selectedSymbol -> contains Action and Reducer to update the currently selected Stock Symbol in the store.
- timeWindow -> contains Action and Reducer to update the Time Window in the store.
- socialMediaTypes (todo) -> contains Action and Reducer to update the list of social media services in the store.
- selectedAlgorithm -> contains Action and Reducer to update the selected algorithm and its paramter inputs if applicable, in the store.